# TECHNICAL PAPER ON HTML AND CSS CONCEPTS 


## ABSTRACT
  Revising the topics encountered in HTML and CSS and noting down importent topics that we would refer for future assisstence.

## TOPICS 

* Box Model
* Inline versus Block Elements. Examples.
* Positioning: Relative/Absolute
* Common CSS structural classes
* Common CSS syling classes
* CSS Specificity
* CSS Responsive Queries
* Flexbox/Grid
* Common header meta tags
<br>

## Box Model

  - The term "box model" is used when talking about design and layout.

  - The CSS box model is essentially a box that wraps around every HTML element that consists of: margins, borders, padding, and the actual content. The image below illustrates the box model
<br>
<br>
    
<img src="https://codinglead.co/images/box-model.png" alt="box model"/>
<br>
<br>
  - Explanation of the different parts:
    *   Content - The content of the box, where text and images appear
    *  Padding - Clears an area around the content. The padding is transparent
    * Border - A border that goes around the padding and content
    * Margin - Clears an area outside the border. The margin is transparent

## Inline versus Block Elements

 1. ### Block-level Elements
    * A block-level element always starts on a new line, and the browsers automatically add some space (a margin) before and after the element.
    * A block-level element always takes up the full width available (stretches out to the left and right as far as it can).
    * Some commonly used block elements are: `<p>` and `<div>`
  2. ### Inline Elements
      * An inline element does not start on a new line.
      * An inline element only takes up as much width as necessary
      * Some commonly used inline elements are: `<span>`,`<button>` .

## Positioning

  1. ### Relative
     * An element with `position: relative;` is positioned relative to its normal position.
     * Setting the top, right, bottom, and left properties of a relatively-positioned element will cause it to be adjusted away from its normal position. Other content will not be adjusted to fit into any gap left by the element.
  2. ### Absolute
     * An element with `position: absolute;` is positioned relative to the nearest positioned ancestor (instead of positioned relative to the viewport, like fixed).
     * If an absolute positioned element has no positioned ancestors, it uses the document body, and moves along with page scrolling.
     * Note: Absolute positioned elements are removed from the normal flow, and can overlap elements.
## Common CSS structural classes
  - structural classes in CSS are those You can position elements to a new location by specifying their horizontal and vertical coordinates or mentioning the height and width of an element.some of the structural classes in CSS are width, height, margin,display, max-height, min-height, max-width, min-width etc.

## Common CSS syling classes
  - structural classes in CSS are those you use for styling an element so that i could look good.with these you can specify the element which style you would prefer. Some of the CSS style classes are color,font-style,font-family,padding,text-decoration,word-space etc


## CSS Specificity 
  * If there are two or more CSS rules that point to the same element, the selector with the highest specificity value will "win", and its style declaration will be applied to that HTML element.
  * Think of specificity as a score/rank that determines which style declaration are ultimately applied to an element.
  * There are four categories which define the specificity level of a selector from top specificity to low specificity:

    * Inline styles - Example: `<h1 style="color: pink;">`
    * IDs - Example: `#navbar`
    * Classes, pseudo-classes, attribute selectors - Example: `.test`, `:hover`, `[href]`
    * Elements and pseudo-elements - Example: `h1`, `:before`
## CSS Responsive Queries 
  * Media query is a CSS technique introduced in CSS3.
  * It uses the @media rule to include a block of CSS properties only if a certain condition is true.
  * The @media rule is used in media queries to apply different styles for different media types/devices.
  * Media queries can be used to check many things, such as:

    * width and height of the viewport
    * width and height of the device
    * orientation (is the tablet/phone in landscape or portrait mode?)
    * resolution
  * Using media queries are a popular technique for delivering a tailored style sheet (responsive web design) to desktops, laptops, tablets, and mobile phones.
  * You can also use media queries to specify that certain styles are only for printed documents or for screen readers (mediatype: print, screen, or speech).
  * In addition to media types, there are also media features. Media features provide more specific details to media queries, by allowing to test for a specific feature of the user agent or display device. For example, you can apply styles to only those screens that are greater, or smaller, than a certain width.
## Flexbox/Grid 
  - Flexbox and CSS Grid are two CSS layout modules that have become mainstream in recent years. Both allow us to create complex layouts that were previously only possible by applying CSS hacks and/or JavaScript. Flexbox and CSS Grid share multiple similarities and many layouts can be solved with both
  1. ### Flexbox
     * Flexbox is the short name for the Flexible Box Layout CSS module, designed to make it easy for us to lay things out in one dimension — either as a row or as a column. To use flexbox, you apply `display: flex;` to the parent element of the elements you want to lay out; all its direct children then become flex items
        - syntax :- `display:flex;`
            
   
  2. ### Grid
     * While flexbox is designed for one-dimensional layout, Grid Layout is designed for two dimensions — lining things up in rows and columns

        - syntax :- `display:grid;`

## Common header meta tags
  - The `<meta>` element is typically used to specify the character set, page description, keywords, author of the document, and viewport settings.The metadata will not be displayed on the page, but it is used by browsers, by search engines, and other web services
  - some meta tags are as follows:
    - Define the character set used
      - syntax :- `<meta charset="UTF-8">`
    - Define keywords for search engines
      - syntax :-`<meta name="keywords" content="HTML, CSS, JavaScript">`
    - Define a description of your web page
      - syntax :-`<meta name="description" content="Free Web tutorials">`
    - Define the author of a page:
      - syntax :-`<meta name="author" content="John Doe">`
    - Refresh document every 30 seconds:
      - syntax :-`<meta http-equiv="refresh" content="30">`
    - Setting the viewport to make your website look good on all devices
      - syntax :-`<meta name="viewport" content="width=device-width, initial-scale=1.0">`

<hr>


## References
  - [w3schools](https://www.w3schools.com/html/)
  - [Tutorialspoint](https://www.tutorialspoint.com/html)
  - [wikipedia](https://en.wikipedia.org/wiki/HTML)